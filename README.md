# docker-lumen
Creating a simple REST API in the Lumen (micro-framework by Laravel) with Docker.

### Docker Setup
- [Docker for Mac](https://docs.docker.com/docker-for-mac/)
- [Docker for Linux](https://docs.docker.com/engine/installation/linux/)
- [Docker for Windows](https://docs.docker.com/docker-for-windows/)

### Docker Basic
Run the command bellow at the root of the project.

```ssh
$ docker-compose up -d
```

Access the app on http://localhost:8080/

### Contribute
Submit a Pull Request!

## install composer dependencies

`docker run --rm -v ${pwd}/www/lumen:/app composer install`

## common laravel commands

`docker-compose exec app php artisan migrate --seed`

`docker-compose exec app php artisan make:controller MyController`

